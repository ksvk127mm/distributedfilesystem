package com.uel;

import javax.swing.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.util.*;

public class Client
{
    private static final int LIST = 1;
    private static final int UPLOAD = 2;
    private static final int RENAME = 3;
    private static final int DELETE = 4;

    private static final int client_id_1 = 0;
    private static final int client_id_2 = 1;
    private static final int client_id_3 = 2;
    private static final int client_id_4 = 3;
    private static final int port_1 = 5000;
    private static final int port_2 = 5001;
    private static final int port_3 = 5002;
    private static final int port_4 = 5003;

    private static int myIndex, myNextIndex;

    private static boolean CONTINUAR = true;
    private static final Queue<DistributedRequest> requestQueue = new LinkedList<>();
    private static final List<ClientService> serviceList = new ArrayList<>();

    public Client(ClientService service)
    {
        serviceList.add(service);
    }

    private static int getPort(int client_id)
    {
        switch (client_id){
            case client_id_1:
                return port_1;
            case client_id_2:
                return port_2;
            case client_id_3:
                return port_3;
            case client_id_4:
                return port_4;
            default:
                return -1;
        }
    }

    private static void setIndexes(int client_id) throws IOException
    {
        switch (client_id){
            case client_id_1:
                myIndex = client_id_1;
                myNextIndex = 1;
                break;
            case client_id_2:
                myIndex = client_id_2;
                myNextIndex = 2;
                break;
            case client_id_3:
                myIndex = client_id_3;
                myNextIndex = 3;
                break;
            case client_id_4:
                myIndex = client_id_4;
                myNextIndex = 0;
                break;
            default:
                throw new IOException("Erro: id de cliente inválido");
        }
    }

    private static void connectToServers()
    {
        try {
            Client.serviceList.add((ClientService) Naming.lookup(("rmi://localhost:" + getPort(client_id_1) + "/servico_" + client_id_1)));
            Client.serviceList.add((ClientService) Naming.lookup(("rmi://localhost:" + getPort(client_id_2) + "/servico_" + client_id_2)));
            Client.serviceList.add((ClientService) Naming.lookup(("rmi://localhost:" + getPort(client_id_3) + "/servico_" + client_id_3)));
            Client.serviceList.add((ClientService) Naming.lookup(("rmi://localhost:" + getPort(client_id_4) + "/servico_" + client_id_4)));
        } catch (Exception ex){
            System.out.println("Exception capturada ao conectar com os servidores: " + ex.getMessage());
            System.exit(-1);
        }
    }

    public static void main(String[] args)
    {
        connectToServers();
        try {
            Scanner input = new Scanner(System.in);
            System.out.print("Digite o id do cliente: ");
            setIndexes(Integer.parseInt(input.nextLine()));
            Thread producerThread = new Thread(new Producer());
            Thread consumerThread = new Thread(new Consumer());
            producerThread.start();
            Thread.sleep(100);
            consumerThread.start();
            producerThread.join();
            consumerThread.join();
        } catch (InterruptedException ex){
            System.out.println("Exception capturada ao iniciar threads: " + ex.getMessage());
            System.exit(-1);
        } catch (IOException ex){
            System.out.println("Exception capturada ao pegar id do cliente: " + ex.getMessage());
            System.exit(-1);
        }
    }

    static class Consumer implements Runnable
    {
        @Override
        public void run()
        {
            while (CONTINUAR) {
                synchronized (Client.class) {
                    while (requestQueue.size() == 0 && CONTINUAR) {
                        try {
                            Client.class.wait();
                        } catch (InterruptedException ex) {
                            System.out.println("Exception ao travar o consumidor: " + ex.getMessage());
                        }
                    }
                    if (requestQueue.size() > 0) {
                        DistributedRequest request = requestQueue.poll();
                        switch (request.getCommand()) {
                            case LIST:
                                try {
                                    ArrayList<String> result = new ArrayList<>();
                                    for(ClientService service: serviceList){
                                        String[] fileNames = service.listFiles();
                                        assert fileNames != null;
                                        if(fileNames.length != 0){
                                            result.addAll(Arrays.asList(fileNames));
                                        }
                                    }
                                    if(result.size() != 0){
                                        for(String name : result){
                                            System.out.println("Nome do arquivo: " + name);
                                        }
                                    } else {
                                        System.out.println("Pastas Vazias");
                                    }
                                } catch (IOException ex) {
                                    System.out.println("Exception ao listar arquivos: " + ex.getMessage());
                                }
                                break;
                            case UPLOAD:
                                try {
                                    boolean fileExists = false;
                                    ArrayList<String> result = new ArrayList<>();
                                    for(ClientService service: serviceList){
                                        String[] fileNames = service.listFiles();
                                        assert fileNames != null;
                                        if(fileNames.length != 0){
                                            result.addAll(Arrays.asList(fileNames));
                                        }
                                    }
                                    if(result.size() != 0){
                                        for(String name : result){
                                            if (name.equals(request.getFileName())) {
                                                fileExists = true;
                                                break;
                                            }
                                        }
                                    }
                                    if(!fileExists){
                                        int smallestSize = Integer.MAX_VALUE;
                                        ClientService chosenServer = null;
                                        for (ClientService service : serviceList) {
                                            int nextSize = service.getDatabaseSize();
                                            if (smallestSize >= nextSize){
                                                smallestSize = nextSize;
                                                chosenServer = service;
                                            }
                                        }
                                        assert chosenServer != null;
                                        if(chosenServer.upload(request.getData(), request.getFileName()))
                                            System.out.println("Arquivo " + request.getFileName() + " criado com sucesso");
                                        else
                                            System.out.println("Erro: falha ao criar o arquivo " + request.getFileName());
                                    } else {
                                        System.out.println("Erro: arquivo " + request.getFileName() + " já existe");
                                    }
                                } catch (IOException ex) {
                                    System.out.println("Exception ao criar arquivo: " + ex.getMessage());
                                }
                                break;
                            case RENAME:
                                try {
                                    boolean fileExists = false;
                                    for(ClientService service: serviceList){
                                        String[] fileNames = service.listFiles();
                                        assert fileNames != null;
                                        if(fileNames.length != 0){
                                            for(String name : fileNames){
                                                if(name.equals(request.getFileName())){
                                                    fileExists = true;
                                                    if(service.renameFile(request.getFileName(), request.getNewName()))
                                                        System.out.println("Arquivo " + request.getFileName() + " renomeado para " + request.getNewName() + " com sucesso");
                                                    else
                                                        System.out.println("Erro: falha ao renomear arquivo " + request.getFileName());
                                                }
                                            }
                                        }
                                    }
                                    if(!fileExists)
                                        System.out.println("Erro: falha ao renomear arquivo: arquivo " + request.getFileName() + " não encontrado");
                                } catch (RemoteException ex) {
                                    System.out.println("Exception ao renomear arquivo: " + ex.getMessage());
                                }
                                break;
                            case DELETE:
                                try {
                                    boolean fileExists = false;
                                    for(ClientService service: serviceList){
                                        String[] fileNames = service.listFiles();
                                        assert fileNames != null;
                                        if(fileNames.length != 0){
                                            for(String name : fileNames){
                                                if(name.equals(request.getFileName())){
                                                    fileExists = true;
                                                    if(service.deleteFile(request.getFileName()))
                                                        System.out.println("Arquivo " + request.getFileName() + " removido com sucesso");
                                                    else
                                                        System.out.println("Erro: falha ao remover arquivo " + request.getFileName());
                                                }
                                            }
                                        }
                                    }
                                    if(!fileExists)
                                        System.out.println("Erro: falha ao remover arquivo: " + request.getFileName() + " não encontrado");
                                } catch (RemoteException ex) {
                                    System.out.println("Exception ao remover arquivo: " + ex.getMessage());
                                }
                                break;
                        }
                    }
                    Client.class.notify();
                }
            }
        }
    }

    static class Producer implements Runnable
    {
        private void listFiles()
        {
            DistributedRequest request = new DistributedRequest();
            request.setCommand(LIST);
            requestQueue.add(request);
        }

        private void upload()
        {
            try {
                JFileChooser chooser = new JFileChooser();
                int result = chooser.showOpenDialog(null);
                if (result == JFileChooser.APPROVE_OPTION) {
                    byte[] data = new byte[(int) chooser.getSelectedFile().length()];
                    FileInputStream input = new FileInputStream(chooser.getSelectedFile());
                    input.read(data, 0, data.length);
                    DistributedRequest request = new DistributedRequest();
                    request.setCommand(UPLOAD);
                    request.setData(data);
                    request.setFileName(chooser.getSelectedFile().getName());
                    requestQueue.add(request);
                }
            } catch(IOException ex){
                System.out.println("Exception ao selecionar o arquivo: " + ex.getMessage());
            }
        }

        private void rename()
        {
            String fileName, newName;
            Scanner input_1 = new Scanner(System.in);
            Scanner input_2 = new Scanner(System.in);

            // TODO -- Descobrir como resolver o problema de nextLine() travando
            System.out.print("Digite o nome do arquivo: ");
            fileName = input_1.nextLine();
            if(fileName.length() == 0){
                System.out.println("Erro: nome de arquivo inválido");
                return;
            }

            System.out.print("Digite o novo nome para o arquivo " + fileName + ": ");
            newName = input_2.nextLine();
            if(newName.length() == 0){
                System.out.println("Erro: nome de arquivo inválido");
                return;
            }

            DistributedRequest request = new DistributedRequest();
            request.setCommand(RENAME);
            request.setFileName(fileName);
            request.setNewName(newName);
            requestQueue.add(request);
        }

        public void delete()
        {
            String fileName;
            Scanner input = new Scanner(System.in);

            System.out.print("Digite o nome do arquivo: ");
            fileName = input.nextLine();
            if(fileName.length() == 0){
                System.out.println("Erro: nome de arquivo inválido");
                return;
            }
            DistributedRequest request = new DistributedRequest();
            request.setCommand(DELETE);
            request.setFileName(fileName);
            requestQueue.add(request);
        }

        @Override
        public void run()
        {
            Scanner input = new Scanner(System.in);
            int result;

            while (CONTINUAR) {
                synchronized (Client.class) {
                    while(requestQueue.size() != 0 && CONTINUAR) {
                        try {
                            Client.class.wait();
                        } catch (InterruptedException ex){
                            System.out.println("Exception ao travar o produtor: " + ex.getMessage());
                        }
                    }
                    System.out.println("-----------------------------------------");
                    System.out.println("    Sistema de Arquivos Distribuído");
                    System.out.println("-----------------------------------------");
                    System.out.println("Escolha sua operação:");
                    System.out.println("1 - Listar arquivos da pasta");
                    System.out.println("2 - Criar arquivo");
                    System.out.println("3 - Remover arquivo");
                    System.out.println("4 - Renomear arquivo");
                    System.out.println("5 - Sair do programa");
                    System.out.print("Escolha: ");
                    try {
                        result = Integer.parseInt(input.nextLine());
                    } catch (NumberFormatException ex) {
                        result = -1;
                    }
                    switch (result) {
                        case 1:
                            listFiles();
                            break;
                        case 2:
                            upload();
                            break;
                        case 3:
                            delete();
                            break;
                        case 4:
                            rename();
                            break;
                        case 5:
                            CONTINUAR = false;
                            break;
                        default:
                            System.out.println("ERRO: entrada invalida");
                            break;
                    }
                    Client.class.notify();
                }
            }
        }
    }
}
