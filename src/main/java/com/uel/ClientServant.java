package com.uel;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class ClientServant extends UnicastRemoteObject implements ClientService
{
    private final int server_id;
    private final String serverPath;
    private final String backupPath;
    private boolean token = true;

    public ClientServant(int server_id) throws RemoteException
    {
        super();
        this.server_id = server_id;
        this.serverPath = ("database_" + server_id + "/");
        this.backupPath = ("backup_" + server_id + "/");
    }

    public int getServer_id() {
        return server_id;
    }

    public void switchToken()
    {
        token = !token;
    }

    public boolean getToken() {
        return token;
    }

    private void verifyRedundancy()
    {
        File backupDir = new File(backupPath);
        String[] backupFileNames = backupDir.list();

        assert backupFileNames != null;
        for(String fileName : backupFileNames){
            File backupFile = new File(backupPath + fileName);
            File serverFile = new File(serverPath + fileName);
            if(serverFile.isFile()){
                try {
                    if (!FileUtils.contentEquals(serverFile, backupFile))
                        System.out.println("Aviso de redundância: arquivo " + fileName + " está diferente do seu backup");
                } catch (IOException ex){
                    System.out.println("Exception ao detectar redundância: " + ex.getMessage());
                }
            } else {
                try {
                    Files.copy(backupFile.toPath(), serverFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    System.out.println("Aviso de redundância: arquivo " + fileName + " foi restaurado");
                } catch (IOException ex) {
                    System.out.println("Exception ao efetuar redundancia: " + ex.getMessage());
                }
            }
        }
    }

    public boolean upload(byte[] data, String fileName) throws RemoteException
    {
        verifyRedundancy();

        boolean flag = false;

        try {
            File serverFilePath = new File(serverPath + fileName);
            File backupFilePath = new File(backupPath + fileName);
            if(!serverFilePath.isFile() && !backupFilePath.isFile()) {
                FileOutputStream output = new FileOutputStream(serverFilePath);
                FileOutputStream backup = new FileOutputStream(backupFilePath);
                output.write(data);
                backup.write(data);
                output.flush();
                backup.flush();
                output.close();
                backup.close();
                flag = true;
            }
        } catch (IOException ex) {
            System.out.println("Exception capturada ao efetuar upload: " + ex.getMessage());
        }
        return flag;
    }

    public int getDatabaseSize() throws RemoteException
    {
        verifyRedundancy();

        File folder = new File(serverPath);
        int length = 0;

        File[] files = folder.listFiles();

        assert files != null;
        for(File file: files){
            if(file.isFile())
                length += file.length();
        }

        return length;
    }

    public String[] listFiles() throws RemoteException
    {
        verifyRedundancy();

        File serverDir = new File(serverPath);
        return serverDir.list();
    }

    public boolean renameFile(String fileName, String newName) throws RemoteException
    {
        verifyRedundancy();

        File serverFile = new File(serverPath + fileName);
        File serverRename = new File(serverPath + newName);
        File backupFile = new File(backupPath + fileName);
        File backupRename = new File(backupPath + newName);

        boolean flag;
        if(serverFile.isFile() && backupFile.isFile())
            flag = serverFile.renameTo(serverRename) && backupFile.renameTo(backupRename);
        else
            flag = false;

        if(flag)
            System.out.println("Arquivo renomeado com sucesso");
        else
            System.out.println("Erro: falha ao renomear arquivo " + fileName);

        return flag;
    }

    public boolean deleteFile(String fileName) throws RemoteException
    {
        verifyRedundancy();

        File serverFile = new File(serverPath + fileName);
        File backupFile = new File(backupPath + fileName);

        boolean flag;

        if(serverFile.isFile() && backupFile.isFile()){
            flag = serverFile.delete() && backupFile.delete();
        } else
            flag = false;

        if(flag)
            System.out.println("Arquivo removido com sucesso");
        else
            System.out.println("Erro: falha ao remover arquivo, arquivo ou backup de " + fileName + " não existe");

        return flag;
    }
}
