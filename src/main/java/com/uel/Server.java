package com.uel;

import java.io.File;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

public class Server
{
    private static final int server_id_1 = 0;
    private static final int server_id_2 = 1;
    private static final int server_id_3 = 2;
    private static final int server_id_4 = 3;
    private static final int port_1 = 5000;
    private static final int port_2 = 5001;
    private static final int port_3 = 5002;
    private static final int port_4 = 5003;

    private static int getPort(int server_id)
    {
        switch (server_id){
            case server_id_1:
                return port_1;
            case server_id_2:
                return port_2;
            case server_id_3:
                return port_3;
            case server_id_4:
                return port_4;
            default:
                return -1;
        }
    }

    public static void main(String[] args) throws RemoteException
    {
        int server_id;
        Scanner input = new Scanner(System.in);
        System.out.print("Digite o id do servidor: ");
        server_id = Integer.parseInt(input.nextLine());

        String serverPath = ("database_" + server_id + "/");
        String backupPath = ("backup_" + server_id + "/");

        File file = new File(serverPath);
        if (!file.exists())
            file.mkdir();

        File backupFile = new File(backupPath);
        if (!backupFile.exists())
            backupFile.mkdir();

        Registry registry = LocateRegistry.createRegistry(getPort(server_id));
        registry.rebind(("servico_" + server_id), new ClientServant(server_id));
    }
}
