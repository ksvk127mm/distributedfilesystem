package com.uel;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ClientService extends Remote
{
    boolean upload(byte[] data, String fileName) throws RemoteException;

    int getDatabaseSize() throws RemoteException;

    String[] listFiles() throws RemoteException;

    boolean renameFile(String fileName, String newName) throws RemoteException;

    boolean deleteFile(String fileName) throws RemoteException;

    void switchToken() throws RemoteException;

    boolean getToken() throws RemoteException;
}
