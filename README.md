Sistema de arquivos distribuído
-
Criar makefile que gere um .jar para o cliente (Cliente.java) e um .jar para o 
servidor (Server.java) com suas respectivas dependências.

Para executar a aplicação devem serem seguidos os seguintes passos na ordem correta:

1 - Executar uma instância de servidor.jar e inserir o id 0

2 - Executar uma instância de servidor.jar e inserir o id 1

3 - Executar uma instância de servidor.jar e inserir o id 2

4 - Executar uma instância de servidor.jar e inserir o id 3

5 - Executar uma instância de cliente.jar e inserir o id 0

6 - Executar uma instância de cliente.jar e inserir o id 1

7 - Executar uma instância de cliente.jar e inserir o id 2

8 - Executar uma instância de cliente.jar e inserir o id 3

Logo temos 4 servidores (middleware) numerados de 0 a 3 e 4 clientes 
(aplicação) numerados de 0 a 3.

Por padrão o endereço localhost está sendo usado, caso queira mudar isso ou usar a entrada padrão
é só fazer mudanças nas funções main() de Server.java e Client.java.

OBS: Não esquecer de baixar e incluir a biblioteca commons.io.jar encontrada em:
https://commons.apache.org/proper/commons-io/download_io.cgi
